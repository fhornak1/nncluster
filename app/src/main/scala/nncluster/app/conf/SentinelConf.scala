package nncluster.app.conf

case class SentinelConf(trainingConcurrency: Int, validationConcurrency: Int)
