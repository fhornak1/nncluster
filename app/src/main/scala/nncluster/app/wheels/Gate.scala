package nncluster.app.wheels

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import scorch.autograd.Variable
import scorch.nn.{Module => NNModule}
import scorch.optim.Optimizer

object Gate {
  sealed trait Command
  final case class Wire(prevRef: ActorRef[Command], nextRef: ActorRef[Command]) extends Command
  final case class Validate(x: Variable, y: Variable) extends Command
  final case class Forward(x: Variable, y: Variable) extends Command
  final case class Backward(g: Variable) extends Command

  def apply(module: NNModule, optimizer: Optimizer): Behavior[Command] =
    Behaviors.setup { ctx =>
      Behaviors.receiveMessagePartial {
        case w: Wire =>
          ctx.log.debug("Received wire {}", w)
          wired(module, optimizer, w, List.empty)
      }
    }

  def wired(module: NNModule, optimizer: Optimizer, wire: Gate.Wire, activations: List[(Variable, Variable)]): Behavior[Command] =
    Behaviors.setup { ctx =>
      Behaviors.receiveMessage {
        case Validate(x, y) =>
          val result = module(x)
          wire.nextRef ! Forward(result, y)
          Behaviors.same
        case Forward(x, y) =>
          val result = module(x)
          wire.nextRef ! Forward(result, y)
          wired(module, optimizer, wire, activations :+ (x, result))
        case Backward(g) =>
          activations match {
            case Nil =>
              ctx.log.error("backward received, but no activations registered")
              Behaviors.same
            case ::((in, out), rest) =>
              optimizer.zeroGrad()
              out.backward(g)
              wire.prevRef ! Backward(in.grad)
              optimizer.step()
              wired(module, optimizer, wire, rest)
          }
        case m =>
          ctx.log.error("Invalid message {} in state wired", m)
          Behaviors.stopped
      }
    }
}
