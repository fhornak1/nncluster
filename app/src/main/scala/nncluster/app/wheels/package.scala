package nncluster.app

import scorch.autograd.Variable

package object wheels {
  type DataIterator = Iterator[(Variable, Variable)]
}
