package nncluster.app.wheels

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ ActorRef, Behavior }
import scorch.autograd.Variable
import scorch.data.loader.DataLoader

object DataProvider {

  case class Batch(
    x: Variable,
    y: Variable)

  object Batch {
    def apply(xy: (Variable, Variable)): Batch = Batch(xy._1, xy._2)
  }

  sealed trait Command

  final case class NextBatch(
    sendTo: ActorRef[Gate.Command],
    toMessage: (Batch) => Gate.Command,
    replyTo: ActorRef[Sentinel.Command])
      extends Command

  def apply(
    name: String,
    dl: DataLoader
  ): Behavior[Command] = {
    val di = dl.iterator
    provideNext(name, dl, di, di.next(), 1, System.currentTimeMillis())
  }

  private def provideNext(
    name: String,
    dl: DataLoader,
    di: DataIterator,
    nextBatch: (Variable, Variable),
    epoch: Int,
    startTime: Long
  ): Behavior[Command] =
    Behaviors.setup { ctx =>
      Behaviors.receiveMessage { case NextBatch(sendTo, toMessage, replyTo) =>
        sendTo ! toMessage(Batch(nextBatch))
        if (di.hasNext)
          provideNext(name, dl, di, di.next(), epoch, startTime)
        else {
          val duration = System.currentTimeMillis() - startTime
          replyTo ! Sentinel.Epoch(name, epoch, duration)
          val nextDataIter = dl.iterator
          provideNext(name, dl, nextDataIter, nextDataIter.next(), epoch + 1, System.currentTimeMillis())
        }
      }
    }
}
