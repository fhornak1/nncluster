package nncluster.app.wheels

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ ActorRef, Behavior }
import nncluster.app.conf.SentinelConf
import nncluster.app.wheels.DataProvider.{ Batch, NextBatch }
import scorch.autograd.Variable
import scorch.data.loader.DataLoader

object Sentinel {
  sealed trait Command

  final case class Wire(
    forwRef: ActorRef[Gate.Command],
    backRef: ActorRef[Gate.Command])
      extends Command

  final case class Forward(
    x: Variable,
    y: Variable)
      extends Command

  final case class Backward(g: Variable) extends Command

  final case class Validate(
    x: Variable,
    y: Variable)
      extends Command

  final case object Start extends Command

  final case class Epoch(
    name: String,
    epoch: Int,
    duration: Long)
      extends Command

  final case class Stats(
    trainingLoss: Double,
    trainingBatches: Long,
    validationLoss: Double,
    validationScore: Double,
    validationBatches: Long) {

    def updateTraining(loss: Variable): Stats =
      copy(trainingLoss = trainingLoss + loss.data.squeeze(), trainingBatches = trainingBatches + 1)

    def updateValidation(
      loss: Variable,
      score: Double
    ): Stats = copy(validationLoss = validationLoss + loss.data.squeeze(), validationScore = validationScore + score)

    def trLoss: Double = trainingLoss / trainingBatches

    def valLoss: Double = validationLoss / validationBatches

    def valScore: Double = validationScore / validationBatches
  }

  object Stats {
    def empty: Stats = Stats(0, 0, 0, 0, 0)
  }

  def apply(
    training: DataLoader,
    validation: DataLoader,
    sc: SentinelConf,
    lossFn: (Variable, Variable) => Variable,
    eval: (Variable, Variable) => Double
  ): Behavior[Command] =
    Behaviors.setup { ctx =>
      val tdl = ctx.spawn(DataProvider("training", training), "training-provider")
      val vdl = ctx.spawn(DataProvider("validation", validation), "validation-provider")

      Behaviors.receiveMessagePartial { case Wire(forwRef, backRef) =>
        sentinel(training, validation, tdl, vdl, sc, lossFn, eval, backRef, forwRef, Stats.empty)
      }
    }

  def sentinel(
    training: DataLoader,
    validation: DataLoader,
    tdl: ActorRef[DataProvider.Command],
    vdl: ActorRef[DataProvider.Command],
    sc: SentinelConf,
    lossFn: (Variable, Variable) => Variable,
    eval: (Variable, Variable) => Double,
    backRef: ActorRef[Gate.Command],
    forwRef: ActorRef[Gate.Command],
    stats: Stats
  ): Behavior[Command] =
    Behaviors.setup { ctx =>
      Behaviors.receiveMessagePartial {
        case Forward(yHat, y) =>
          val l = lossFn(yHat, y)
          l.backward()
          backRef ! Gate.Backward(yHat.grad)
          sentinel(training, validation, tdl, vdl, sc, lossFn, eval, backRef, forwRef, stats.updateTraining(l))
        case Backward(_) =>
          tdl ! NextBatch(forwRef, batchToTrain, ctx.self)
          Behaviors.same
        case Validate(x, y) =>
          if (stats.validationBatches + 1 < validation.numBatches)
            vdl ! NextBatch(forwRef, batchToValidate, ctx.self)
          sentinel(
            training,
            validation,
            tdl,
            vdl,
            sc,
            lossFn,
            eval,
            backRef,
            forwRef,
            stats.updateValidation(lossFn(x, y), eval(x, y)))
        case Start =>
          1 to sc.trainingConcurrency foreach (_ => tdl ! NextBatch(forwRef, batchToTrain, ctx.self))
          (1 to sc.validationConcurrency) foreach (_ => vdl ! NextBatch(forwRef, batchToValidate, ctx.self))
          Behaviors.same
        case Epoch("training", epoch, duration) =>
          ctx
            .log
            .info(
              "Epoch: {}; TrnLoss: {}; ValLoss: {}, ValScore: {}, Duration: {} [ms]",
              epoch.asInstanceOf[Object],
              stats.trLoss.asInstanceOf[Object],
              stats.valLoss.asInstanceOf[Object],
              stats.valScore.asInstanceOf[Object],
              duration.asInstanceOf[Object]
            )
          1 to sc.validationConcurrency foreach (_ => vdl ! NextBatch(forwRef, batchToValidate, ctx.self))
          sentinel(training, validation, tdl, vdl, sc, lossFn, eval, backRef, forwRef, Stats.empty)
        case Epoch("validation", epoch, duration) =>
          ctx.log.info("Ignored")
          Behaviors.same
      }
    }

  def batchToTrain(b: Batch): Gate.Command = Gate.Forward(b.x, b.y)

  def batchToValidate(b: Batch): Gate.Command = Gate.Validate(b.x, b.y)

}
