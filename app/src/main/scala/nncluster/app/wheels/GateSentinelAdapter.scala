package nncluster.app.wheels

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors

object GateSentinelAdapter {

  def apply(sentRef: ActorRef[Sentinel.Command]): Behavior[Gate.Command] =
    Behaviors.setup { ctx =>
      ctx.log.info("Spawning sentinel-gate adaptor")
      Behaviors.receiveMessage {
        case Gate.Wire(prevRef, nextRef) =>
          sentRef ! Sentinel.Wire(prevRef, nextRef)
          Behaviors.same
        case Gate.Validate(x, y) => sentRef ! Sentinel.Validate(x, y)
          Behaviors.same
        case Gate.Forward(x, y) =>
          sentRef ! Sentinel.Forward(x, y)
          Behaviors.same
        case Gate.Backward(g) =>
          sentRef ! Sentinel.Backward(g)
          Behaviors.same
      }
    }

}
